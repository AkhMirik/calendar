/**
 * Generate array of numbers between given range
 *
 * @param {Number} start
 * @param {Number} end
 *
 * @return Array<Number>
 */
export const range = (start, end) => {
    return Array(end - start + 1).fill().map((_, idx) => start + idx)
}
